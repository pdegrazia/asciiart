import unittest
from ascii_art import AsciiArt, BASE_URL
import random
import requests


class AsciiArtTests(unittest.TestCase):
    def setUp(self):
        # making sure service is up
        #if it is not, stop whole execution
        try:
            response = requests.get(BASE_URL, timeout=2)
            self.assertEqual(response.status_code, 200)
        except Exception as e:  # too broad, must redefine it
            self.fail()

        self.ascii_art = AsciiArt()

    def test_instance(self):
        self.assertTrue(isinstance(self.ascii_art, AsciiArt))

    def test_font_list_acquired(self):
        assert(len(self.ascii_art.font_list) > 0)

    def test_get_font_sample(self):
        font_list_length = len(self.ascii_art.font_list)
        font = self.ascii_art.font_list[random.randint(0, font_list_length)]
        text = self.ascii_art.get_font_sample(font)
        self.assertTrue(len(text) > 0)
        self.assertTrue(isinstance(text, unicode))

    def test_i_feel_lucky(self):
        text = self.ascii_art.i_feel_lucky('random text')
        self.assertTrue(len(text) > 0)
        self.assertTrue(isinstance(text, unicode))