import random
import requests


BASE_URL= 'http://artii.herokuapp.com'
font_list = []


class AsciiArt(object):
    def __init__(self):
        self.font_list = self.__set_font_list()

    def __set_font_list(self):
        response = requests.get(BASE_URL+'/fonts_list')
        return str.split(str(response.text))

    def print_font_list(self):
        print '\033[1mList of fonts\033[0m'
        print('\n'.join(self.font_list))

    def get_font_sample(self, font_name):
        response = requests.get(BASE_URL+'/make?text=Hello!&font=%s' % font_name)
        return response.text

    def i_feel_lucky(self, text):
        font_name = self.font_list[random.randint(0, len(self.font_list))]
        print(font_name)
        response = requests.get(BASE_URL + '/make?text=%s&font=%s' % (text, font_name))
        return response.text

if __name__ == '__main__':
    ascii_art = AsciiArt()
    ascii_art.print_font_list()
    while True:
        text = raw_input('Insert the word:')
        ascii_art.i_feel_lucky(text)

