===Wrapper for ASCII Art API===

Dependencies

python (2 or 3)

requests


Wrote a simple API to send some text and get it back as ascii art with a random font

Usage:
1. Standalone mode: just run ascii_art.py, insert some text, it will return the text in ascii art with a random font
2. Module:

```
from ascii_art import AsciiArt
aa = AsciiArt()
aa.i_feel_lucky()

```

tested with unittest